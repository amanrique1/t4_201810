package model.vo;

import java.util.Iterator;

import model.data_structures.ILinkedList;
import model.data_structures.ListaDoblementeEncadenada;

public class Compania implements Comparable<Compania> {

	private String nombre;

	private ListaDoblementeEncadenada<Taxi> taxisInscritos;	

	private ListaDoblementeEncadenada<Servicio> serviciosAsociados;	
	
	private ListaDoblementeEncadenada<InfoTaxiRango> infoTaxisEnRango;	
 


	public Compania(String nombre)
	{
		taxisInscritos=new ListaDoblementeEncadenada<Taxi>();
		infoTaxisEnRango=new ListaDoblementeEncadenada<InfoTaxiRango>();
		serviciosAsociados=new ListaDoblementeEncadenada<Servicio>();
		this.nombre=nombre;
	}
	public Compania(String nombre, InfoTaxiRango infoTaxi)
	{
		taxisInscritos=new ListaDoblementeEncadenada<Taxi>();
		serviciosAsociados=new ListaDoblementeEncadenada<Servicio>();
		infoTaxisEnRango=new ListaDoblementeEncadenada<InfoTaxiRango>(infoTaxi);
		serviciosAsociados.enlazar(infoTaxi.getServiciosPrestadosEnRango());

		this.nombre=nombre;
	}
	public Compania(String nombre, Taxi taxi, Servicio servicio)
	{
		taxisInscritos=new ListaDoblementeEncadenada<Taxi>(taxi);
		serviciosAsociados=new ListaDoblementeEncadenada<Servicio>(servicio);
		infoTaxisEnRango=new ListaDoblementeEncadenada<InfoTaxiRango>();
		this.nombre=nombre;
	}


	public void addInfoTaxiRango(InfoTaxiRango infoTaxi)
	{
		
		infoTaxisEnRango.add(infoTaxi);
		serviciosAsociados.enlazar(infoTaxi.getServiciosPrestadosEnRango());

	}

	public void addServicio(Servicio servicio)
	{
		serviciosAsociados.add(servicio);
	}


	public ListaDoblementeEncadenada<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(ListaDoblementeEncadenada<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public void addTaxi(Taxi taxi)
	{
		taxisInscritos.add(taxi);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ListaDoblementeEncadenada<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango)
	{
		Iterator<Taxi> iterTaxis=taxisInscritos.iterator();
		Taxi max=null;
		double maxTotal=0.0;
		while(iterTaxis.hasNext())
		{
			Taxi actual = iterTaxis.next();
			double total=actual.darGananciaEnRango(rango);
			if(maxTotal<total)
			{
				maxTotal=total;
				max=actual;
			}
		}

		return max;

	}

	public Taxi darTaxiConMasServEnRango(RangoFechaHora rango)
	{
		Iterator<Taxi> iterTaxis=taxisInscritos.iterator();
		Taxi max=null;
		int mayor=0;
		while(iterTaxis.hasNext())
		{
			Taxi actual = iterTaxis.next();
			int totActual=actual.darServEnRango(rango);
			if(mayor<totActual)
			{
				mayor=totActual;
				max=actual;
			}
		}

		return max;
	}
	public Taxi darTaxiMasEfectivo()
	{
		Iterator<Taxi> iterTaxis=taxisInscritos.iterator();
		Taxi max=null;
		double mayor=0;
		double actualTaxi=0;
		while(iterTaxis.hasNext())
		{
			Taxi actual = iterTaxis.next();

			if(actual.getMillas()!=0)
				
				actualTaxi=actual.getDinero()/actual.getMillas();
			else
			{
				actualTaxi=0;
			}
			if(mayor<=actualTaxi)
			{
				mayor=actualTaxi;
				max=actual;
			}
		}

		return max;
	}
	
	
	public void acomodarServiciosARango(RangoFechaHora rango)
	{
		Iterator<Servicio> iter=serviciosAsociados.iterator();
		String fechaInicio= rango.getFechaInicial();
		String fechaFin= rango.getFechaFinal();
		String horaInicio= rango.getHoraInicio();
		String horaFin= rango.getHoraFinal();
		int i=-1;
		int f=-1;
		boolean ya=false;
		while(!ya&&iter.hasNext())
		{
			
			i++;
			Servicio actual=iter.next();
			String fechaActual=actual.getFecha().getFechaInicial();
			if(fechaActual.compareTo(fechaInicio)>=0&&fechaActual.compareTo(fechaFin)<=0)
			{
				while(!ya)
				{
					String horaActual=actual.getFecha().getHoraInicio();
					fechaActual=actual.getFecha().getFechaInicial();
					if(fechaActual.compareTo(fechaInicio)==0)
					{
						while(f==-1&& fechaActual.compareTo(fechaFin)<=0&&horaActual.compareTo(horaFin)<0)
						{
							
							if((horaActual.compareTo(horaInicio)>=0||fechaActual.compareTo(fechaInicio)>0))
							{	
								f=i;
								
								if((fechaFin.compareTo(fechaInicio)==0&&horaActual.compareTo(horaFin)>=0))
									{
										f=-1;
										ya=true;
									}
						
								
								if(iter.hasNext())
									actual=iter.next();
								else
									ya=true;
								
							}
							else if(iter.hasNext())
							{
								i++;
								actual=iter.next();
								horaActual=actual.getFecha().getHoraInicio();
								fechaActual=actual.getFecha().getFechaInicial();
							}
							else
							{
								f=-2;
								ya=true;
							}
	
						}
						
					}
					if(!ya&&fechaActual.compareTo(fechaFin)==0)
					{
						if(f==-1)
							f=i;
					
						while(!ya&&actual.getFecha().getHoraInicio().compareTo(horaFin)<0)
						{
							f++;
							if(iter.hasNext())
								actual=iter.next();
							else
								ya=true;
						}
						ya=true;
							
					}
					else if(!ya)
					{
						if(f==-1)
							f=i;
						while(iter.hasNext()&&(actual=iter.next()).getFecha().getFechaInicial().compareTo(fechaFin)<0)
							f++;
						if(iter.hasNext())
							actual=iter.next();
						else
							ya=true;
					}
				}
				
			}
			
			
		}	
		if(i>f)
			serviciosAsociados.limpiar();
		else
			serviciosAsociados=serviciosAsociados.subList(i, f);
	}

	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return nombre.compareTo(o.getNombre());
	}










}
