package model.vo;

import java.util.Iterator;

import model.data_structures.ListaDoblementeEncadenada;

public class FechaServicios implements Comparable<FechaServicios>{
	
	private String fecha;
	private ListaDoblementeEncadenada<Servicio> serviciosAsociados;
	private int numServicios;
	
	public FechaServicios(Servicio servicio)
	{
		serviciosAsociados=new ListaDoblementeEncadenada<Servicio>(servicio);
		fecha=servicio.getFecha().getFechaInicial();
		numServicios=1;
	}
	
	public void addServicio(Servicio servicio)
	{
		serviciosAsociados.add(servicio);
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public ListaDoblementeEncadenada<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(ListaDoblementeEncadenada<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public int getNumServicios() {
		numServicios=serviciosAsociados.size();
		return numServicios;
	}
	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	
	public void acomodarARango(RangoFechaHora rango, int estado)
	{
		Iterator<Servicio> iter=serviciosAsociados.iterator();
		String inicio= rango.getHoraInicio();
		String fin= rango.getHoraFinal();
		int f=-1;
		int i=-1;
		boolean ya=false;
		while(iter.hasNext()&&!ya)
		{
			String horaInicioActual=iter.next().getFecha().getHoraInicio();
			i++;
			if(horaInicioActual.compareTo(inicio)>=0&&horaInicioActual.compareTo(fin)<0)
			{
				f=i;
				
				while(iter.hasNext()&&iter.next().getFecha().getHoraInicio().compareTo(fin)<0)
					f++;
				ya=true;
			}
			if(horaInicioActual.compareTo(fin)>=0)
				ya=true;
		
		}	
		if(f<i)
		{
			serviciosAsociados.limpiar();
			return;
		}

		switch(estado)
		{
			case 0:
				serviciosAsociados=serviciosAsociados.subList(i, serviciosAsociados.size()-1);
				return;
			case 1:
				serviciosAsociados=serviciosAsociados.subList(0, f);
				return;
			case 2:
				serviciosAsociados=serviciosAsociados.subList(i, f);
				return;
		}

	}
	
	
	@Override
	public int compareTo(FechaServicios o) {
		// TODO Auto-generated method stub
		return fecha.compareTo(o.getFecha());
	}
	
	

}
