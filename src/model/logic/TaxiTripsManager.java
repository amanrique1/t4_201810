package model.logic;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Comparator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.PriorityQueueListaDoble;
import model.data_structures.ILinkedList;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;

import model.vo.ClassManager;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.ServiceVo;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";
	public static final String DIRECCION_02= "/taxi-trips-wrvz-psew-subset-02-02-2017.json";
	public static final String DIRECCION_03= "/taxi-trips-wrvz-psew-subset-03-02-2017.json";
	public static final String DIRECCION_04= "/taxi-trips-wrvz-psew-subset-04-02-2017.json";
	public static final String DIRECCION_05= "/taxi-trips-wrvz-psew-subset-05-02-2017.json";
	public static final String DIRECCION_06= "/taxi-trips-wrvz-psew-subset-06-02-2017.json";
	public static final String DIRECCION_07= "/taxi-trips-wrvz-psew-subset-07-02-2017.json";
	public static final String DIRECCION_08= "/taxi-trips-wrvz-psew-subset-08-02-2017.json";






	private ClassManager administrador;





	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		try
		{

			administrador=new ClassManager();
			if(direccionJson==DIRECCION_LARGE_JSON)
				cargarLarge();
			else
				cargarSmallMedium(direccionJson);


		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}


		return true;
	}


	public void cargarSmallMedium(String direccionJson) throws Exception
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json
		InputStream inputStream = new FileInputStream(direccionJson);

		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		int contador=0;
		
		int contador0=0;
		int contador1=0;
		int contador2=0;

		
		
		while (reader.hasNext())
		{
			// Instancia un servicio del json
			ServiceVo nServ = gson.fromJson(reader, ServiceVo.class);
		
			
			String fechaHoraInicial=nServ.getTrip_start_timestamp()==null?"":nServ.getTrip_start_timestamp();
			String fechaHoraFinal=nServ.getTrip_end_timestamp()==null?"":nServ.getTrip_end_timestamp();
			
			administrador.agregarServicio(1,nServ.getTrip_id()==null?"":nServ.getTrip_id(),
					nServ.getTaxi_id()==null?"":nServ.getTaxi_id(),
					nServ.getTrip_seconds()==null?"0":nServ.getTrip_seconds(),
					nServ.getTrip_miles()==null?"0":nServ.getTrip_miles(),
					nServ.getTrip_total()==null? "0":nServ.getTrip_total(),
					fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(0,fechaHoraInicial.indexOf("T")),
					fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(fechaHoraInicial.indexOf("T")+1),
					fechaHoraFinal.equals("")?"9999-99-99":fechaHoraFinal.substring(0,fechaHoraFinal.indexOf("T")),
					fechaHoraFinal.equals("")?"99:99:99.999":fechaHoraFinal.substring(fechaHoraFinal.indexOf("T")+1),
					nServ.getCompany()==null? "Independent Owner":nServ.getCompany(),
					nServ.getPickup_community_area()==null?"-1":nServ.getPickup_community_area(),
					nServ.getDropoff_community_area()==null?"-1":nServ.getDropoff_community_area());
			contador++;
			
			String inicio=nServ.getPickup_community_area()==null?"-1":nServ.getPickup_community_area();
			String finali=nServ.getDropoff_community_area()==null?"-1":nServ.getDropoff_community_area();
			
			
			if(finali.equals("6")&&inicio.equals("6"))
				contador2++;
			else if(finali.equals("6"))
				contador1++;
			else if(inicio.equals("6"))
				contador0++;

		}
		administrador.agregarCompaniasTaxisZonas();

		
		
		System.out.println("Se cargaron "+contador+" servicios");
		
		System.out.println("Se guardaron en la lista "+administrador.getCompanias().size()+ " Compa�ias en total");
		System.out.println("Se guardaron en la lista "+administrador.getTaxis().size()+ " Taxis en total");
		System.out.println("Se guardaron en la lista "+administrador.getServicios().size()+ " Servicios en total");

		System.out.println("inician: "+contador0);
		System.out.println("terminan: "+contador1);
		System.out.println("inician y terminan: "+contador2);

		

		reader.close();
	}


	public void cargarLarge() throws Exception
	{
		String direccion="";
		int contador=0;
		int contador0=0;
		int contador11=0;
		int contador2=0;
		
		
		for(int i=1;i<8;i++)
		{
			if(i==1)
				direccion=DIRECCION_02;
			else if(i==2)
				direccion=DIRECCION_03;
			else if(i==3)
				direccion=DIRECCION_04;
			else if(i==4)
				direccion=DIRECCION_05;
			else if(i==5)
				direccion=DIRECCION_06;
			else if(i==6)
				direccion=DIRECCION_07;
			else
				direccion=DIRECCION_08;

			int contador1=0;

			//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json
			InputStream inputStream = new FileInputStream(DIRECCION_LARGE_JSON+direccion);

			//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

			JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
			Gson gson = new GsonBuilder().create();

			// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
			reader.beginArray();
			while (reader.hasNext())
			{

				// Instancia un servicio del json
				ServiceVo nServ = gson.fromJson(reader, ServiceVo.class);
				

				String fechaHoraInicial=nServ.getTrip_start_timestamp()==null?"":nServ.getTrip_start_timestamp();
				String fechaHoraFinal=nServ.getTrip_end_timestamp()==null?"":nServ.getTrip_end_timestamp();
				
				administrador.agregarServicio(i,nServ.getTrip_id()==null?"":nServ.getTrip_id(),
						nServ.getTaxi_id()==null?"":nServ.getTaxi_id(),
						nServ.getTrip_seconds()==null?"0":nServ.getTrip_seconds(),
						nServ.getTrip_miles()==null?"0":nServ.getTrip_miles(),
						nServ.getTrip_total()==null? "0":nServ.getTrip_total(),
						fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(0,fechaHoraInicial.indexOf("T")),
						fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(fechaHoraInicial.indexOf("T")+1),
						fechaHoraFinal.equals("")?"9999-99-99":fechaHoraFinal.substring(0,fechaHoraFinal.indexOf("T")),
						fechaHoraFinal.equals("")?"99:99:99.999":fechaHoraFinal.substring(fechaHoraFinal.indexOf("T")+1),
						nServ.getCompany()==null? "Independent Owner":nServ.getCompany(),
						nServ.getPickup_community_area()==null?"-1":nServ.getPickup_community_area(),
						nServ.getDropoff_community_area()==null?"-1":nServ.getDropoff_community_area());
				
//				if(nServ.getCompany()!=null&&nServ.getCompany().equals("Taxi Affiliation Services")
//						&&fechaHoraInicial.substring(0,fechaHoraInicial.indexOf("T")).compareTo("2017-02-02")>=0
//						&&fechaHoraFinal.substring(0,fechaHoraFinal.indexOf("T")).compareTo("2017-02-09")<=0)
//				{
//					System.out.println(nServ.getTaxi_id().substring(0, 15)+" :"+nServ.getTrip_total());
//				}
					
				
				contador++;
				contador1++;
					
//				String fecha= fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(0,fechaHoraInicial.indexOf("T"));
	
				String inicio=nServ.getPickup_community_area()==null?"-1":nServ.getPickup_community_area();
				String finali=nServ.getDropoff_community_area()==null?"-1":nServ.getDropoff_community_area();

				if(finali.equals("6")&&inicio.equals("6"))
					contador2++;
				else if(finali.equals("6"))
					contador11++;
				else if(inicio.equals("6"))
					contador0++;
				
//			if(inicio.equals("32")&&fecha.equals("2017-02-06"))
//				contador11++;
				

			}
			System.out.println("Se cargaron "+contador1+" servicios");
			if(i>1)
				administrador.agregarServicio(0,null, null, null, null, null, null,null, null, null, null, null,null);


			reader.close();

		}
		administrador.agregarCompaniasTaxisZonas();
		
		
		
		System.out.println("Hay "+contador+ " servivios en total");
		

		System.out.println("Se guardaron en la lista "+administrador.getCompanias().size()+ " Compa�ias en total");
		System.out.println("Se guardaron en la lista "+administrador.getTaxis().size()+ " Taxis en total");
		System.out.println("Se guardaron en la lista "+administrador.getServicios().size()+ " Servicios en total");

//		System.out.println("El 2017-02-06 en la zona 32: "+contador11);
		
//		System.out.println("inician: "+contador0);
//		System.out.println("terminan: "+contador11);
//		System.out.println("inician y terminan: "+contador2);
		
	}






	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return administrador.darServiciosEnPeriodo(rango);
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company) throws Exception
	{
		// TODO Auto-generated method stub
		return administrador.darTaxiConMasServiciosEnCompaniaYRango(rango, company);
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango) throws Exception
	{
		// TODO Auto-generated method stub
		return administrador.darInformacionTaxiEnRango(id, rango);
	}

	@Override //4A
	public ListaDoblementeEncadenada<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		// TODO Auto-generated method stub
		return administrador.darListaRangosDistancia(fecha, horaInicial, horaFinal);
	}

	@Override //1B
	public ListaDoblementeEncadenada<Compania> darCompaniasTaxisInscritos() 
	{
		// TODO Auto-generated method stub


		return administrador.darCompaniasTaxisInscritos();
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) throws Exception
	{
		// TODO Auto-generated method stub
		return administrador.darTaxiMayorFacturacion(rango, nomCompania);
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		// TODO Auto-generated method stub
		return administrador.darServiciosZonaValorTotal(rango, idZona);
	}

	@Override //4B
	public ListaDoblementeEncadenada<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return administrador.darZonasServicios(rango);
	}

	@Override //2C
	public ListaDoblementeEncadenada<Compania> companiasMasServicios(RangoFechaHora rango, int n)throws Exception
	{
		// TODO Auto-generated method stub
		return administrador.companiasMasServicios(rango, n);
	}

	@Override //3C
	public ListaDoblementeEncadenada<CompaniaTaxi> taxisMasRentables()
	{
		// TODO Auto-generated method stub
		return administrador.taxisMasRentables();
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		return administrador.darServicioResumen(taxiId, horaInicial, horaFinal, fecha);
	}
	public InfoTaxiRango[] darTaxisEnPeriodo(RangoFechaHora rango) throws Exception
	{
		return administrador.darTaxisEnPeriodo(rango);
	}
	
	public InfoTaxiRango[] darTaxisEnPeriodoOrdenados(RangoFechaHora rango)throws Exception
	{
		return administrador.darTaxisEnPeriodoOrdenados(rango);

	}
	public PriorityQueueListaDoble<Compania> companiasTaxisEnPeriodo(RangoFechaHora rango) throws Exception
	{
		return administrador.companiasTaxisEnPeriodo(rango);
	}


}
